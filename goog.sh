#!/bin/bash

if [[(-n "$1") && (-n "$2") && (-z "$3")]]; then
    wget -q -O temp.html $2
    num=$(sed 's/$1/$1\n/g' temp.html | grep -c $1)
    echo "$1: $num"
else
    echo "Usage goog.sh WORD WEBSITE"
fi

