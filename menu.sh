#!/bin/bash

printmenu(){
  echo "v) View $1"
  echo "e) Edit $1"
  echo "c) Compile $1"
  echo "q) Quit"
}

option(){
  case $1 in
       v)
          less $2
          ;;
       e)
          jpico $2
          ;;
       c)
          g++ $2
          ;;
       q)
          break
          ;;
       *)
          echo "Invalid Option. Skipping file: $2"
          ;;
  esac
}

for file in *; do
  (printmenu $file)
  read input
  (option $input $file)
done
